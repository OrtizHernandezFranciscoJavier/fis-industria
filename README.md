# Mapa 1
```plantuml
@startmindmap
*[#lightPink] Industria 4.0
	*[#ORANGE] En la actualidad todos hablan sobre la industria 4.0
		*[#GREEN] Podemos encontrar informacion sobre ella en: \n *Articulos cientificos \n *Periodicos \n *TV \n *Libros \n * Foros y conferencias 
	*[#ORANGE] La cuarta revolucion industrial tiene gran importancia en la sociedad mundial.
		*[#GREEN] Esta involucra muchas tecnologias como: \n *La robotica \n *Big data \n *Computacion en la nube \n *Sistemas autonomos \n *Internet de las cosas \n *Inteligencia artificial
	*[#ORANGE] Historia de la industria.
		*[#GREEN] Industria 1.0 
			*[#Yellow] "Primera revolucion industrial" Comenzo en 1784 \n se caracterizo por la produccion basada en maquinaria \n y por la aparicion de transportes de carga masiva \n como el tren a vapor posible gracias a la invencion de la \n energia a vapor y de la energia hidraulica.
				*[#lightgreen] 	Estos inventos permitieron que la cantidad de \n produccion incrementaran increiblemente y que el costo \n de los productos bajara de anera considerada. 
					*[#lightyellow] La produccion de hierro en Reino Unido aumento de 30,000 Toneladas a 1 millon \n para el año de 1810
		*[#GREEN] Industria 2.0
			*[#YELLOW] Se cacacterizo por la invencion de la energia electrica y \n la linea de ensamblaje
				*[#lightgreen] Estas mejoras hicieron posible la produccion en masa e \n hizo que los precios de los productos bajara considerablemente.  
					*[#lightYellow] Gracias la linea de ensamblaje iniciada por Henry Ford hizo que la produccion de \n los carros aumentara en 800 % y que su precio disminuyera \n casi a la tercera parte de su precio inicial.
		*[#GREEN] Industria 3.0
			*[#YELLOW] A partir de de 1969 inicia, estuvo caracterizada por la produccion \n automatizada de productos. La cual fue posible mediante la tecnologia de la electronica \n y de la tecnologia de la informacion.
				*[#lightGreen] Las personas comenzaron a trabajar con computadoras \n y con robots basicos.
		*[#GREEN] Industria 4.0
			*[#YELLOW] Se caracteriza por un sistema de produccion inteligente con decision autonoma, es decir \n que las computadoras y las maquinas vallan tomando sus propias decisiones en el \n proceso de produccion. Existiendo cada vez menos intervencion humana.
				*[#lightgreen] Esto es posible gracias al desarrollo de nuevas tecnologias como: \n*IoT \n*Big Data \n*Cloud \n*Robot \n*Etc... 	
					*[#lightYellow] Aunque la industria 4.0 comenzo como un termino para \n describir las nuevas tendencias de las industrias de produccion, actualmente se nos utiliza como sinonimo de transformacion \n digital
						*[#lightblue] Es decir como el proceso por el cual las empresas \n y la sociedad organizan sus metodos de trabajo y estrategias para \n obtener mas beneficios gracias a la implementacion de las nuevas tecnologias. 
	
	*[#ORANGE] ¿Que cambios va a generar?
		*[#PINK] Primer cambio: \n La integracion de TIC'S en la industria de manufactura y de servicios
			*[#YELLOW] Un ejemplo de esto es las fabricas automoviles tesla   la cual cuenta con \n *Procesos automatizados \n Cada vez menor participacion humana. 	
				*[#lightgreen] Esto es posible gracias a la aparicion de uesvas tecnologias como \n*Sistemas de sensores \n*Internet de las cosas \n*Redes de alta velocidad \n*Aprendizaje maquina \n*Big Data \n*Robots autonomos
			*[#Yellow] En la industria 4.0 va mas alla del proceso de manufactura	
				*[#lightgreen] El desarrollo de robots y carros inteligentes esta permitiendo \n que la automatizacion se implemente en todo el proceso de la cadena \n de suministros incluyendo el manejo de los inventarios \n y la entrega de los productos
					*[#lightYellow] El inventario de las bodegas de amazon son abastecidos \n por pequeños robots que se mueven autonomamente para cumplir \n con los pedidos de los clientes
			*[#YELLOW] Otras empresas estan utilentoncesizando los avances tecnologicos para \n incorporar la automatizacion en la entrega de productos.				
				*[#lightgreen] La empresa NURO a desarrollado unos vehiculos autonomos de tamaño reducido \n para realizar la entrega de productos a los consumidores.
	*[#Orange] ¿Que pasara con los puestos de trabajo? 
		*[#GREEN] Algunos afirman que no hay problema, ya que /n apareceran nuevos trabajos.		
			*[#YELLOW] Si nos basamos en las estadisticas de las investigaciones la creacion de los \n nuevos puestos de trabajo es muy limitada a comparacion de los que \n se estan eliminando. Esto no solo ocurre en las empresas de manufactura tambien \n esta pasando en otros tipos de negocios.
			*[#YELLOW] El proceso de automatizacion en diferentes tipos \n de industrias esta trayendo consigo efectos.
			*[#YELLOW] Reduccion de los puestos de trabajo \n*47% de los trabajos actuales desapareceran en los proximos \n 25 años \nPaulatinamente iran desapareciendo puestos de trabajos como: \n*Modelos \n*Arbitro \n*Jueces \n*Personal de telemercadeo \n*Taxistas \n*Cajeros de bancos \n*Pescadores \n*Cocineros de comida rapida  
			*[#YELLOW] La inteligencia artificial se ah desarrolado a un \n punto que ya es posible mantener una conversacion con \n una persona y entender sus intenciones \n*Pilotos de drones
			*[#YELLOW] Aplicando esta tecnologia al area de los servicios muchos mas puestos \n de trabajo estarian en peligro de desaparecer. 
		*[#GREEN] Aparicion de nueva profesiones.\n hace 10 años no teniamos profesiones como:
			*[#YELLOW] *Youtubers \n* Conductores de uber \n*Desarrolladores de apps \n*Consultores de redes sociales
				*[#lightgreen] 65% de los estudiantes de primaria tendran profesiones \n que no existen en la actualidad \n 85% de los trabajos a existir en 2030, no han sido inventados todavia.
		*[#PINK] Transformacion de las empresas de manufactura en \n empresas en TIC'S
			*[#YELLOW] Hace algunas decadas atras los electrodomesticos eran productos sencillos que tenian un fin \n muy especifico
				*[#lightgreen] En la actualidad los electrodomesticos tienen un alcance mucho mayor 
					*[#lightyellow] Estos cuentan con: \n*Un sistema operativo \n*Procesamiento de lenguaje natural \n*Computacion en la nube \n*Comunicacion inalambrica \n*Software 
						*[#lightblue] Cada vez existen menos diferenciacion entre las industrias
		*[#PINK] Nuevos paradigmas y tecnologias 
			*[#YELLOW] Velocidad
				*[#lightgreen] En el nuevo mundo no es el pez grande el que se come al pez \n pequeño sino es el pez rapido el que se come al pez lento
					*[#lightyellow] Si las empresas no se adaptan a los rapidos cambios \n tecnologicos estan estan destinadas a desaparecer.
						*[#lightblue] En los ultimos 15 años han desaparecido el 52% \n de las compañias del fortune 500 \n Ejemplos: \*Nokia \n*Tower records \n*Kodak \n*Blockbuster 
			*[#YELLOW] Negocios basados en plataformas
				*[#lightgreen] Muchas empresas estan creciendo rapidamente basando sus ventas en plataformas tecnologicas \n que unen a partes interesadas como provedores con consumidores. 
					*[#lightyellow] FANG
						*[#lightblue] Son empresas que basados en el activo de a informacion y acompañados \ de las nuevas tecnologias estan generando grandes ingresos economicos
							*[#lightblue] *Amazon \n*Facebook \n*Netflix \n*Google
					*[#lightyellow] BAT
						*[#lightblue] *Baidu \n*Alibaba \n*Tencent
			*[#YELLOw] Internet de las cosas 
				*[#lightgreen] Los sistemas de internet de las cosas van a permitir que los \n diferentes dispositivos se comuniquen unos con otros con la finalidad de \n lograr la automatizacion de diferentes procesos y entregar una vida mas \n confortable a las personas
					*[#lightyellow] Las estimaciones para el año 2020 eran: \n*4000 millones de personas conectadas a internet \n*30000-50000 millones de dispositivos conectados a internet \n todo esto generaria 4.4 zettabytes de datos.	
			*[#YELLOW] Big Data e inteligencia artificial
				*[#lightgreen] El recurso mas valioso del mundo no es el petroleo son \n los datos 
					*[#lightyellow] Hace refencia a que quien acumule y procese la mayor cantidad de \n datos va a dominar el mundo de los negocios.
						*[#lightblue] La inteligencia artificial sera la nueva electricidad, asi como \n la electricidad transformo al mundo enla segunda revolucion industrial, la inteligencia\n artificial transformara al mundo en esta era.
					*[#lightyellow] El machine learnig y la inteligencia artificial a tenidp un progreso\n muy importante en los ultimos años gracias a los datos generados
						*[#lightblue] 2012 proyecto Deep learning \n*Entre Andrew NG y Google \n*1600 procesadores y Deep Neural Network \n*10 Millones de videos de perros y gatos \n*En tres dias reconocio a los gatos.
						*[#lightblue] El uso de la inteligencia artificial se esta extendiendo rapidamente 
							*[#lightblue] Ejemplos: \n*El sistema llamado watson de IBM esta siendo utilizado \n en el campo oncologico de la medicina, siendo capaz de detertar los \n diferentes tipos de tumores de manera mas eficiente \n*Parte financiera  
				*[#lightgreen] Otras tecnologias
					*[#lightyellow] *Carros conectados y autonomos \n*Realidad virtual \n*Blockchain \n*Las comunicaciones 5g \n*Genetica 
		*[#PINK] Nuevas culturas digitales
			*[#YELLOW] Se dice que estamos viviendo la era del Phono Sapiens
				*[#lightgreen] Los phono sapiens son aquellas personas que estan \n acostumbradas a usar el telefono todo el tiempo\n, la mayoria son milenials y generacion z, \n nacidos en las ultimas tres decadas, La tecnologia es parte \n normal de su vida. 
					*[#lightyellow] El 75% de las personas usan un dispositivo digital mientras \n ven la TV.			
	*[#ORANGE] ¿Estamos preparados para estos cambios?
		*[#GREEN] LA RESPUESTA A ESTA PREGUNTA DEPENDERA DE CADA UNO DE NOSOTROS.
@endmindmap
```
# Mapa 2
```plantuml
@startmindmap
*[#Pink] México y la Industria 4.0
	*[#ORANGE] El concepto de la industria 4.0
		*[#GREEN] Consiste en la introduccion de las tecnologias digitales en las \n fabricas con sus..
			
			*[#YELLOW] Inconvenientes
				*[#lightgreen] Perdida de empleo
					*[#lightyellow] En el futuro va haber dos tipos de empleos: \n*Las personas que les digan a las maquinas que hacer \n*Las personas que reciban las ordenes de las maquinas	
						*[#lightblue] Lo que pasa con los empleos es que suben su nivel en cuestiones de valor.
				*[#lightgreen] Seguridad
					*[#lightyellow] Cuando todo esta conectado, un hacker podria tomar \n control de la fabrica inteligente y poder hacer los productos que \n quiera o poder mandar los productos a donde ellos quieran. 
	*[#ORANGE] Start up México 
		*[#GREEN] Primer campus especializado en inovacion y emprendimiento en México 
			*[#YELLOW] En este lugar se encuentran los principales actores del \n ecosistema emprendedor, fondos de inversion, gobierno, emprendedores \n y empresas para unir esfuerzos con el fin de crear empresas \n innovadoras y existosas desde México hacia el mundo.
		*[#GREEN] Saben que la industria 4.0 va a cambiar por completo los negocios. \n La adaptacion de las tecnologias relacionadas con la cuarta revolucion industrial \n permitira a las empresas un ahorro de costos de hasta el 30%.
	*[#ORANGE] ¿Donde esta Mexico dentro de la industria 4.0?
		*[#GREEN] Mexico se encuentra en el punto de inflexion, esto gracias la cercania\n con una de las principales economias del mundo, la gran relacion\n industrial con grandes empresas.
			*[#Yellow] Tenemos una gran oportunidad si Mexico sabe aprovechar \n y empezar a mover todo lo que son las tendencias de educacion, \n tendencias de oportunidades.     		
				*[#lightgreen] Como poder crear a los perfiles laborales que se requieren en el futuro para que \n las empresas que ya tienen aqui sus operaciones como la: \n*Automotriz \n*Aeroespacial \n*Software \n puedan ver al pais como un lugar propicio para poder crecer sus \n industrias, sus empresas. 
		*[#GREEN] Startup "Sin llaves".
			*[#YELLOW] A traves de las cosas estan desarrollando una tecnologia para facilitar \n el dia a dia de las personas y empresas.
				*[#lightgreen] Permite al usuario controlar cualquier acceso ya sea \n privado o publico desde el celular.Algo que podria hacer desaparecer el \n uso de las llaves.
					*[#lightyellow] La aplicacion funciona a partir de la conexion entre \n el celular y el dispositivo que inventaron, que puede adherirse a cualquier \n lado y tiene el tamaño de un puño.
						*[#lightblue] Los dispositivos cuentan con tecnologia IOT, la idea es \n estar siempre conectados a la red generando datos. Los principales beneficios son: \n*seguridad \n*Comodidad.
		*[#GREEN] Startup "We Are Robot" 
			*[#YELLOW] Crear un futuro donde la movilidad sea accesible para todos.
				*[#lightgreen] Fabricacion de exo esqueletos hechos a la medida,conecta al \n medico con el paciente a traves de una aplicacion y hace la terapia fisica\n mas llevadera con la realidad virtual.
					*[#lightyellow] Tras años de estudio patentaron unos exoesqueletos modulares \n para cada parte especifica del cuerpo que se pueden conectar entre \n ellos permitiendo que el dispositivo se pueda extender a un exo\n de cuerpo completo el cual se controlara mediante señales musculares y cerebrales.
						*[#lightblue] Aplicacion en la industria, un exoesqueleto para la reduccion de carga \n en un porcentaje aproximadamente del 20% 
						*[#lightblue] Recopilacion de los datos de operarios.
		   	    				
	
		
@endmindmap